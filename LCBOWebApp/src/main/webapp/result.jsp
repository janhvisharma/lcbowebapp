<%@ page import ="java.util.*" %>
<!DOCTYPE html>
<html>
<head>
	<script>
		function goBack(){
			window.history.back();
		}
	</script>
	<style>
		body, h1, h2, h3, h4, h5, h6 {
			font-family: Arial, sans-serif;
		}
	</style>
</head>
<body>
<center>
<h1>
    Available Brands
</h1>
<%
List result= (List) request.getAttribute("brands");
Iterator it = result.iterator();
out.println("<br>At LCBO, We have <br><br>");
while(it.hasNext()){
out.println(it.next()+"<br>");
}
%>
<button onClick="goBack()">Go Back</button>
</body>
</html>